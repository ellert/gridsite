Source: gridsite
Section: net
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 apache2-dev (>= 2.4),
 libxml2-dev,
 libcurl4-openssl-dev,
 apache2-ssl-dev (>= 2.4.27-3~),
 libcanl-c-dev (>= 3.0.0),
 zlib1g-dev,
 gsoap,
 libgsoap-dev,
 libtool,
 libtool-bin,
 doxygen,
 sharutils,
 dh-apache2,
 pkgconf
Rules-Requires-Root: binary-targets
Standards-Version: 4.6.2
Homepage: https://github.com/CESNET/gridsite
Vcs-Git: https://salsa.debian.org/ellert/gridsite.git
Vcs-Browser: https://salsa.debian.org/ellert/gridsite

Package: gridsite
Architecture: any
Multi-Arch: foreign
Depends:
 libgridsite6t64 (= ${binary:Version}),
 apache2-bin (>= 2.4),
 ${shlibs:Depends},
 ${misc:Depends}
Recommends: ${misc:Recommends}
Description: Grid Security for the Web, Web platforms for Grids
 GridSite was originally a web application developed for managing and
 formatting the content of the http://www.gridpp.ac.uk/ website. Over
 the past years it has grown into a set of extensions to the Apache
 web server and a toolkit for Grid credentials, GACL access control
 lists and HTTP(S) protocol operations.
 .
 This package, gridsite, contains Apache httpd modules for enabling
 mod_gridsite.

Package: libgridsite6t64
Provides: ${t64:Provides}
Replaces: libgridsite6
Breaks: libgridsite6 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Run time libraries for mod_gridsite and gridsite-clients
 GridSite was originally a web application developed for managing and
 formatting the content of the http://www.gridpp.ac.uk/ website. Over
 the past years it has grown into a set of extensions to the Apache
 web server and a toolkit for Grid credentials, GACL access control
 lists and HTTP(S) protocol operations.
 .
 This package contains the runtime libraries.

Package: gridsite-clients
Architecture: any
Multi-Arch: foreign
Depends:
 libgridsite6t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Description: Clients to gridsite: htcp, htrm, htmv
 GridSite was originally a web application developed for managing and
 formatting the content of the http://www.gridpp.ac.uk/ website. Over
 the past years it has grown into a set of extensions to the Apache
 web server and a toolkit for Grid credentials, GACL access control
 lists and HTTP(S) protocol operations.
 .
 This package, gridsite-clients, contains clients for using against
 gridsite, htcp, htrm, ...

Package: libgridsite-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libgridsite6t64 (= ${binary:Version}),
 ${misc:Depends}
Description: Developers tools for gridsite
 GridSite was originally a web application developed for managing and
 formatting the content of the http://www.gridpp.ac.uk/ website. Over
 the past years it has grown into a set of extensions to the Apache
 web server and a toolkit for Grid credentials, GACL access control
 lists and HTTP(S) protocol operations.
 .
 This package, libgridsite-dev, contains developer tools for using
 gridsite.

Package: gridsite-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends}
Description: Developers Documentation for gridsite
 GridSite was originally a web application developed for managing and
 formatting the content of the http://www.gridpp.ac.uk/ website. Over
 the past years it has grown into a set of extensions to the Apache
 web server and a toolkit for Grid credentials, GACL access control
 lists and HTTP(S) protocol operations.
 .
 This package, gridsite-doc, contains developer documentation for
 gridsite.
